<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\admin\Login;
use Auth;

class LoginController extends Controller
{
    //

    function login(Request $req)
    { 
    	$email	  = $req->input('email');
    	$password = $req->input('password');

    	$validated = $req->validate([
        'email' => 'required|email|',
        'password' => 'required|min:6',
    ]);

    	$userData  = array(

    		'email'		=> $req->get('email'),
    		'password'	=> $req->get('password')
    	);

    	$user = Auth::user();
// $password = bcrypt('123456');;
    	if(Auth::attempt($userData))
    	{ 

    		$req->session()->put('userId', $user->id);
    		$req->session()->put('userEmail', $user->email);
    		
    		//echo session('userEmail');
    		return redirect('admin/dashboard');
    	}
    	else
    	{
    		return back()->with('loginMessage','Invaild Login Details.');
    		
    	}
    
    	
    }

}
