<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\Admin\ProfileModel;
use DB;
use Auth;

class ProfileController extends Controller
{
    //

    function adminProfile()
    {
    	$userId =  session('userId');

    	$siteContent =  ProfileModel::find($userId);

    	return view('admin/profile/edit')->with('siteContent',$siteContent);
    }
    function update(Request $request,$id)
	{
		$firstName	  = $request->input('firstName'); 
    	$lastName     = $request->input('lastName');
    	$email 		  = $request->input('email');
    	$contctNumber = $request->input('contctNumber');

    	$validated = $request->validate([
        'firstName' => 'required',
        'lastName'  => 'required',
        'email'     => 'required|email',
        'contctNumber'     => 'required|max:10',
        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
    ]);
    	$imageName = time().'.'.$request->image->extension();  
     
        $request->image->move(public_path('images'), $imageName);
  
    	
    
       $save=  DB::table('users')->where('id', $id)->update(array('first_name' => $firstName, 'last_name' => $lastName, 'email' => $email, 'contact_number' => $contctNumber, 'profile_image' => $imageName, 'modified_date' => time()));

    	
    	if($save)
    	{
    		return redirect("admin/profile")->with('SuccessMessage','Record Successfully enter.');
    	}


	}

	function changePassword()
	{
		$userId = session('userId'); 

		return view("admin/profile/password");
	}

	public function updatePassword(Request $request)
    {

        $request->validate([
            'oldPassword' => 'required',
            'newPassword' => 'required',
            'confirmPassword' => 'same:newPassword',
        ]);

        $hashedPassword = Auth::user()->password;
 
       if (\Hash::check($request->oldPassword , $hashedPassword )) {
 
         if (!\Hash::check($request->newPassword , $hashedPassword)) {
 
              $users =ProfileModel::find(Auth::user()->id);
              $users->password = bcrypt($request->newPassword);
              ProfileModel::where( 'id' , Auth::user()->id)->update( array( 'password' =>  $users->password));
 
              return redirect("admin/profile/change-password")->with('SuccessMessage','Password change successfully.');
            }
 
            else{
                  return redirect("admin/profile/change-password")->with('errorMessage','new password can not be the old password!');
                }
 
           }
 
          else{
               return redirect("admin/profile/change-password")->with('errorMessage','old password doesnt matched');
             }
 
       }

     function removeImage($id)
     {
     	$image = DB::table('users')->where('id', $id)->first();
        $file= $image->profile_image;
        $filename = public_path().'/images/'.$file;
        unlink( $filename);
        

     	$save=  DB::table('users')->where('id', $id)->update(array( 'profile_image' => ''));

    	
    	if($save)
    	{
    		return redirect("admin/profile")->with('SuccessMessage','Record Successfully enter.');
    	}
     }
}
