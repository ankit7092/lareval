<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\SitepageModel;
use Illuminate\Support\Str;
use DB;


class SitepageController extends Controller
{
    //
	function list()
	{
		$siteContent =  SitepageModel::all()->sortByDesc("id");

		return view('admin/sitepage/list',['content'=>$siteContent]);

	}

	function updateStatus($id, $status)
	{
		if($status == 1)
		{
			$status = '0';
			 DB::table('sitepages')->where('id', $id)->update(array('status' => $status));

		}
		else
		{
			$status = '1';
			DB::table('sitepages')->where('id', $id)->update(array('status' => $status));
		}
		return back()->with('SuccessMessage','Status Successfully updated.');
	}

	function addNew(Request $request)
	{
	 	$pageName	  = $request->input('pageName'); 
    	$description  = $request->input('description');
    	$slug 		  = Str::slug($pageName);
    	$status       = '1';
    	$modified	  =	time();

    	$validated = $request->validate([
        'description' => 'required',
        'pageName' => 'required|unique:sitepages,title',
    ]);
    	
    	$sitepages[] = [
           
            'title'=>$pageName,
            'description'=>$description,
            'status'=>'1',
            'modified'=>time(),
            'slug'=>$slug,
        ]; 

    	$save=   DB::table('sitepages')->insert($sitepages);
    	if($save)
    	{
    		return redirect("admin/sitepage/list")->with('SuccessMessage','Record Successfully enter.');
    	}

		//return view('admin/sitepage/add');
	}

	function edit($id)
	{
		$siteContent['PageTitle']	=	'Edit Page';
		$siteContent =  SitepageModel::find($id);

		return view('admin/sitepage/edit')->with('siteContent',$siteContent);
	}

	function updatePage(Request $request,$id)
	{
		$pageName	  = $request->input('pageName'); 
    	$description  = $request->input('description');
    	$slug 		  = Str::slug($pageName);
    	$status       = '1';
    	$modified	  =	time();

    	$validated = $request->validate([
        'description' => 'required',
        'pageName' => 'required',
    ]);
    	
    
       $save=  DB::table('sitepages')->where('id', $id)->update(array('title' => $pageName, 'description' => $description, 'modified'=>time(),  'slug'=>$slug));

    	
    	if($save)
    	{
    		return redirect("admin/sitepage/list")->with('SuccessMessage','Record Successfully enter.');
    	}


	}

	function delete($id)
	{
		$save = DB::table('sitepages')->where('id', $id)->delete();
		if($save)
    	{
    		return redirect("admin/sitepage/list")->with('SuccessMessage','Record Successfully delete.');
    	}
	}
}
