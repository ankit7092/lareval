<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\UserModel;
use Illuminate\Support\Str;
use DB;

class UserController extends Controller
{
    //
    function list()
	{
		$userContent =  DB::table('users')->where('role', 'user')->get()->sortByDesc("id");
			
		return view('admin/user/list',['userContent'=>$userContent]);

	}

	function addNew(Request $request)
	{ 
		 
	 	$firstName	   = $request->input('firstName'); 
    	$lastName 	   = $request->input('lastName');
    	$emailAddress  = $request->input('emailAddress');
    	$cNumber  	   = $request->input('cNumber');
    	$password  	   = $request->input('password');
    	$cPassword     = $request->input('cPasswordcPassword');
    	$image         = $request->file('image');
    	$role          = 'user';
    	$modified	   =  time();

    	$validated   = $request->validate([
        'firstName' => 'required',
        'lastName' => 'required',
        'emailAddress' => 'required|unique:users,email|email',
        'cNumber' => 'required|min:10',
        'password' => 'required',
        'cPassword' => 'same:password',
         'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
    ]);
    	
    	

        $imageName = time().'.'.$request->image->extension();  
     
        $request->image->move(public_path('user'), $imageName);

        $sitepages[] = [
           
            'first_name'=>$firstName,
            'last_name'=>$lastName,
            'user_name'=>'',
            'email'=>$emailAddress,
            'password'=>bcrypt($password),
            'contact_number'=>$cNumber,
            'profile_image'=>$imageName,
            'role'=>$role,
            'status'=>'1',
            'modified_date'=>time(),
            'created_date'=>time(),
            
        ]; 

    	$save=   DB::table('users')->insert($sitepages);
    	if($save)
    	{
    		return redirect("admin/user/list")->with('SuccessMessage','Record Successfully enter.');
    	}

		//return view('admin/sitepage/add');
	}

	function edit($id)
	{
		$siteContent['PageTitle']	=	'Edit User';
		$siteContent =  UserModel::find($id);

		return view('admin/user/edit')->with('siteContent',$siteContent);
	}

	function updatePage(Request $request,$id)
	{
		$firstName	   = $request->input('firstName'); 
    	$lastName 	   = $request->input('lastName');
    	$emailAddress  = $request->input('emailAddress');
    	$cNumber  	   = $request->input('cNumber');
    	$password  	   = $request->input('password');
    	$cPassword     = $request->input('cPasswordcPassword');
    	$image         = $request->file('image');
    	$status       = '1';
    	$modified	  =	time();

		 if($password != '') 
		 {
			$validated = $request->validate([
			        
			        'password' => 'required',
			        'cPassword' => 'same:password',
			        
			       //  'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
			    ]);
		 }
		 if($image != '') 
		 {
			$validated = $request->validate([
			      'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
			    ]);
		 }
    	$validated = $request->validate([
        'firstName' => 'required',
        'lastName' => 'required',
       // 'emailAddress' => 'required|unique:users,email|email',
        'cNumber' => 'required|min:10',
        //'password' => 'required',
        '//cPassword' => 'same:password',
        
       //  'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
    ]);
    	if($password != '')
    	{
    		 $save=  DB::table('users')->where('id', $id)->update(array('password' => bcrypt($password)));
    	}
    	if($image != '')
    	{
    		$imageName = time().'.'.$request->image->extension();  
     
            $request->image->move(public_path('user'), $imageName);

            $save=  DB::table('users')->where('id', $id)->update(array('profile_image' => $imageName));
    	}
    	
    
       $save=  DB::table('users')->where('id', $id)->update(array('first_name' => $firstName, 'last_name' => $lastName, 'contact_number'=>$cNumber, 'modified_date'=>time()));

    	
    	if($save)
    	{
    		return redirect("admin/user/list")->with('SuccessMessage','Record Successfully updated.');
    	}


	}

	function removeImage($id)
     {
     	$image = DB::table('users')->where('id', $id)->first();
        $file= $image->profile_image;
        $filename = public_path().'/user/'.$file;
        unlink( $filename);
        

     	$save=  DB::table('users')->where('id', $id)->update(array( 'profile_image' => ''));

    	
    	if($save)
    	{
    		return back()->with('SuccessMessage','Image successfully remove.');
    	}
     }

    function updateStatus($id, $status)
	{
		if($status == 1)
		{
			$status = '0';
			 DB::table('users')->where('id', $id)->update(array('status' => $status));

		}
		else
		{
			$status = '1';
			DB::table('users')->where('id', $id)->update(array('status' => $status));
		}
		return back()->with('SuccessMessage','Status Successfully updated.');
	}

	function delete($id)
	{
		$save = DB::table('users')->where('id', $id)->delete();
		if($save)
    	{
    		return redirect("admin/user/list")->with('SuccessMessage','Record Successfully delete.');
    	}
	}
}
