<?php

namespace App\Models\admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SitepageModel extends Model
{
    use HasFactory;
    public $table = 'sitepages';
    public $timestamps = false;
}
