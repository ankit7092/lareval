<x-header />
<div class="content-wrapper">
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Update {{ $siteContent->first_name .' '. $siteContent->last_name }} Profile</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="/admin/dashboard">Home</a></li>
          <li class="breadcrumb-item active">{{ $siteContent->first_name .' '. $siteContent->last_name }} Profile</li>
         
        </ol>
      </div>
    </div>
  </div>
  <!-- /.container-fluid --> 
</section>
<section class="content">
  <div class="container-fluid">
    <div class="row"> 
      <!-- left column -->
      <div class="col-md-12"> 
        <!-- general form elements -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Edit Profile </h3>
          </div>
          <!-- /.card-header --> 
          <!-- form start -->
          <form role="form" name="editPage" id="editPage" action="{{url('/admin/profile/update/'.session('userId'))}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
              <div class="form-group">
                <label for="firstName">First Name</label>
                <input type="text" class="form-control" id="firstName" name="firstName" value="{{ old('firstName', $siteContent->first_name)}}" >
              </div>
              <div class="form-group">
                <label for="lastName">Last Name</label>
                <input type="text" class="form-control" id="lastName" name="lastName" value="{{ old('lastName',$siteContent->last_name) }}" >
              </div>
               <div class="form-group">
                <label for="email">Email Address</label>
                <input type="email" class="form-control" id="email" name="email" value="{{ old('email', $siteContent->email ) }}" readonly="">
              </div>
               <div class="form-group">
                <label for="contctNumber">Contact Number</label>
                <input type="text" class="form-control" id="contctNumber" name="contctNumber" value="{{  old('contctNumber',$siteContent->contact_number ) }}" >
              </div>
              <div class="form-group">
                <label for="contctNumber">Profile Image</label>
                @if($siteContent->profile_image !='')
                    <img src="/images/{{$siteContent->profile_image}}" class="img-thumbnail" style="width: 150px;height: 150px;"  />&nbsp;&nbsp; &nbsp;  <a href="/admin/profile/removeImage/{{ $siteContent->id }}">Remove Image</a>
                @else
                    <input type="file" class="form-control" id="image" name="image"  style="border: none;">
                @endif
              </div>


            </div>
            <!-- /.card-body -->
            
            <div class="card-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
        <!-- /.card --> 
        
      </div>
      <!-- /.card --> 
      
    </div>
  </div>
  <!-- /.row -->
  </div>
  <!-- /.container-fluid --> 
</section>
</div>
<x-footer />
