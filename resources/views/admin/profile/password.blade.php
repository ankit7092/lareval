<x-header />
<div class="content-wrapper">
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Change Profile Password</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="/admin/dashboard">Home</a></li>
          <li class="breadcrumb-item active">Change Profile Password</li>
         
        </ol>
      </div>
    </div>
  </div>
  <!-- /.container-fluid --> 
</section>
<section class="content">
  <div class="container-fluid">
    <div class="row"> 
      <!-- left column -->
      <div class="col-md-12"> 
        <!-- general form elements -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Change Profile Password </h3>
          </div>
          <!-- /.card-header --> 
          <!-- form start -->
          <form role="form" name="editPage" id="editPage" action="{{url('/admin/profile/updatePassword')}}" method="post">
            @csrf
            <div class="card-body">
              <div class="form-group">
                <label for="oldPassword">Old Password</label>
                <input type="password" class="form-control" id="oldPassword" name="oldPassword"  >
              </div>
              <div class="form-group">
                <label for="newPassword">New Password</label>
                <input type="password" class="form-control" id="newPassword" name="newPassword"  >
              </div>
               <div class="form-group">
                <label for="confirmPassword">Confirm Password</label>
                <input type="password" class="form-control" id="confirmPassword" name="confirmPassword">
              </div>
             

            </div>
            <!-- /.card-body -->
            
            <div class="card-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
        <!-- /.card --> 
        
      </div>
      <!-- /.card --> 
      
    </div>
  </div>
  <!-- /.row -->
  </div>
  <!-- /.container-fluid --> 
</section>
</div>
<x-footer />
