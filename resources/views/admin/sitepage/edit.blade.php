<x-header />
<div class="content-wrapper">
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Site Pages</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="/admin/dashboard">Home</a></li>
          <li class="breadcrumb-item"><a href="/admin/sitepage/list">Static Pages</a></li>
          <li class="breadcrumb-item active">Edit {{ $siteContent->title }} Page</li>
        </ol>
      </div>
    </div>
  </div>
  <!-- /.container-fluid --> 
</section>
<section class="content">
  <div class="container-fluid">
    <div class="row"> 
      <!-- left column -->
      <div class="col-md-12"> 
        <!-- general form elements -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Edit {{ $siteContent->title }} Page</h3>
          </div>
          <!-- /.card-header --> 
          <!-- form start -->
          <form role="form" name="editPage" id="editPage" action="{{url('/admin/sitepage/updatePage/'.$siteContent->id)}}" method="post">
            @csrf
            <div class="card-body">
              <div class="form-group">
                <label for="pageName">Page Name</label>
                <input type="text" class="form-control" id="pageName" name="pageName" value="{{  old('pageName', $siteContent->title ) }}" readonly="readonly">
              </div>
              <div class="form-group">
                <label for="description">Page Description</label>
                <textarea class="textarea"  name="description" id="description" 
                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ old('description',$siteContent->description ) }}</textarea>
              </div>
            </div>
            <!-- /.card-body -->
            
            <div class="card-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
        <!-- /.card --> 
        
      </div>
      <!-- /.card --> 
      
    </div>
  </div>
  <!-- /.row -->
  </div>
  <!-- /.container-fluid --> 
</section>
</div>
<x-footer />
