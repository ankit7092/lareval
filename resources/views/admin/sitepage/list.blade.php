<x-header />

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Site Pages</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/admin/dashboard">Home</a></li>
              <li class="breadcrumb-item active">Site Pages</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Manage All Static Pages Here.</h3>
                <br><br>
                <a class="btn btn-primary btn-sm" href="/admin/sitepage/add-new">
                              <i class="fas fa-plus">
                              </i>
                              Add New
                          </a>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example2" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>Page Title</th>
                    <th>Status</th>
                    <th>Modified Date</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                   @php($i=1)
                     @foreach ($content as $key => $value)
                    
                     
                  <tr>
                    <td>{{$i}}</td>
                    <td>{{$value->title}}</td>
                    <td>
                      @if($value->status == 0)
                      <a href="/admin/sitepage/updateStatus/{{$value->id}}/{{$value->status}}" class="badge badge-danger">Inactive</a>
                      @endif
                      @if($value->status == 1)
                       <a href="/admin/sitepage/updateStatus/{{$value->id}}/{{$value->status}}" class="badge badge-success">Active</a>
                       @endif
                     </td>
                    <td> {{date('M/d/Y h:i:s A',$value->modified)}}</td>
                    <td> 
                          <a class="btn btn-info btn-sm" href="/admin/sitepage/edit/{{$value->id}}">
                              <i class="fas fa-pencil-alt">
                              </i>
                              Edit
                          </a>
                          <a class="btn btn-danger btn-sm" href="/admin/sitepage/delete/{{$value->id}}" onclick="return confirm('Are you sure you want to delete this Record ?');">
                              <i class="fas fa-trash">
                              </i>
                              Delete
                          </a></td>
                  </tr>
                 @php($i++)
                  @endforeach
                  
                  </tbody>
                 
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
 <x-footer />
