<x-header />
<div class="content-wrapper">
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>User's</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="/admin/dashboard">Home</a></li>
          <li class="breadcrumb-item"><a href="/admin/sitepage/list">User Pages</a></li>
          <li class="breadcrumb-item active">Add New User</li>
        </ol>
      </div>
    </div>
  </div>
  <!-- /.container-fluid --> 
</section>
<section class="content">
  <div class="container-fluid">
    <div class="row"> 
      <!-- left column -->
      <div class="col-md-12"> 
        <!-- general form elements -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Add New</h3>
          </div>
          <!-- /.card-header --> 
          <!-- form start -->
          <form role="form" name="addNewPage" id="addNewPage" action="/admin/user/add-new" method="post" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
              <div class="form-group">
                <label for="pageName">First Name</label>
                <input type="text" class="form-control" id="firstName" name="firstName" value="{{ old('firstName') }}" >
              </div>
              <div class="form-group">
                <label for="pageName">Last Name</label>
                <input type="text" class="form-control" id="lastName" name="lastName" value="{{ old('lastName') }}" >
              </div>
              <div class="form-group">
                <label for="pageName">Email Address</label>
                <input type="email" class="form-control" id="emailAddress" name="emailAddress" value="{{ old('emailAddress') }}" >
              </div>
              <div class="form-group">
                <label for="pageName">Contact Number</label>
                <input type="text" class="form-control" id="cNumber" name="cNumber" value="{{ old('cNumber') }}" >
              </div>
              <div class="form-group">
                <label for="pageName">Password</label>
                <input type="password" class="form-control" id="password" name="password" value="" >
              </div>
              <div class="form-group">
                <label for="pageName">Confirm Password</label>
                <input type="password" class="form-control" id="cPassword" name="cPassword" value="" >
              </div>
              <div class="form-group">
                <label for="pageName">Profile Image</label>
                <input type="file" id="image" name="image" >
              </div>
            </div>
            <!-- /.card-body -->
            
            <div class="card-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
        <!-- /.card --> 
        
      </div>
      <!-- /.card --> 
      
    </div>
  </div>
  <!-- /.row -->
  </div>
  <!-- /.container-fluid --> 
</section>
</div>
<x-footer />
