<x-header />
<div class="content-wrapper">
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>User's</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="/admin/dashboard">Home</a></li>
          <li class="breadcrumb-item"><a href="/admin/user/list">User Page</a></li>
          <li class="breadcrumb-item active">Edit {{$siteContent->first_name .' ' . $siteContent->last_name}} Profile</li>
        </ol>
      </div>
    </div>
  </div>
  <!-- /.container-fluid --> 
</section>
<section class="content">
  <div class="container-fluid">
    <div class="row"> 
      <!-- left column -->
      <div class="col-md-12"> 
        <!-- general form elements -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Edit <strong>{{$siteContent->first_name .' ' . $siteContent->last_name}}</strong> Profile</h3>
          </div>
          <!-- /.card-header --> 
          <!-- form start -->
           <form role="form" name="editUser" id="editUser" action="{{url('/admin/user/updatePage/'.$siteContent->id)}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
              <div class="form-group">
                <label for="pageName">First Name</label>
                <input type="text" class="form-control" id="firstName" name="firstName" value="{{ old('firstName', $siteContent->first_name) }}" >
              </div>
              <div class="form-group">
                <label for="pageName">Last Name</label>
                <input type="text" class="form-control" id="lastName" name="lastName" value="{{ old('lastName', $siteContent->last_name) }}" >
              </div>
              <div class="form-group">
                <label for="pageName">Email Address</label>
                <input type="email" class="form-control" id="emailAddress" name="emailAddress" value="{{ old('emailAddress', $siteContent->email) }}" readonly >
              </div>
              <div class="form-group">
                <label for="pageName">Contact Number</label>
                <input type="text" class="form-control" id="cNumber" name="cNumber" value="{{ old('cNumber', $siteContent->contact_number) }}" >
              </div>
              <div class="form-group">
                <label for="pageName">Password</label>
                <input type="password" class="form-control" id="password" name="password" value="" >
              </div>
              <div class="form-group">
                <label for="pageName">Confirm Password</label>
                <input type="password" class="form-control" id="cPassword" name="cPassword" value="" >
              </div>
              <div class="form-group">
                <label for="pageName">Profile Image</label>
                 @if($siteContent->profile_image !='')
                    <img src="/user/{{$siteContent->profile_image}}" class="img-thumbnail" style="width: 150px;height: 150px;"  />&nbsp;&nbsp; &nbsp;  <a href="/admin/user/removeImage/{{ $siteContent->id }}">Remove Image</a>
                @else
                    <input type="file" class="form-control" id="image" name="image"  style="border: none;">
                @endif
              </div>
            </div>
            <!-- /.card-body -->
            
            <div class="card-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
        <!-- /.card --> 
        
      </div>
      <!-- /.card --> 
      
    </div>
  </div>
  <!-- /.row -->
  </div>
  <!-- /.container-fluid --> 
</section>
</div>
<x-footer />
