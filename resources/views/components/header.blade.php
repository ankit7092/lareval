<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>AdminLTE 3 | Dashboard</title>
<!-- Tell the browser to be responsive to screen width -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Font Awesome -->
<link rel="stylesheet" href="{{asset('admin/plugins/fontawesome-free/css/all.min.css')}}">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- Tempusdominus Bbootstrap 4 -->
<link rel="stylesheet" href="{{asset('admin/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
<!-- iCheck -->
<link rel="stylesheet" href="{{asset('admin/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
<!-- JQVMap -->
<link rel="stylesheet" href="{{asset('admin/plugins/jqvmap/jqvmap.min.css')}}">
<!-- Theme style -->
<link rel="stylesheet" href="{{asset('admin/dist/css/adminlte.min.css')}}">
<!-- overlayScrollbars -->
<link rel="stylesheet" href="{{asset('admin/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
<!-- Daterange picker -->
<link rel="stylesheet" href="{{asset('admin/plugins/daterangepicker/daterangepicker.css')}}">

<!-- DataTables -->
<link rel="stylesheet" href="{{asset('admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('admin/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset ( 'admin/plugins/toastr/toastr.min.css') }}">
<!-- summernote -->
<link rel="stylesheet" href="{{asset('admin/plugins/summernote/summernote-bs4.css')}}">
<!-- Google Font: Source Sans Pro -->
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4"> 
  <!-- Brand Logo --> 
  <a href="#" class="brand-link"> <img src="{{ asset('admin/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8"> <span class="brand-text font-weight-light">Admin Panel</span> </a> 
  
  <!-- Sidebar -->
  <div class="sidebar"> 
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image"> <img src="{{ asset('admin/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image"> </div>
      <div class="info"> <a href="#" class="d-block">{{ session('userEmail')}}</a> </div>
    </div>
    
    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <li class="nav-item has-treeview"> <a href="#" class="nav-link"> <i class="nav-icon fas fa-book"></i>
          <p> Pages <i class="fas fa-angle-left right"></i> </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item"> <a href="/admin/sitepage/list" class="nav-link"> <i class="far fa-circle nav-icon"></i>
              <p>Site Pages</p>
              </a> </li>
          </ul>
        </li>
        <li class="nav-item has-treeview"> <a href="#" class="nav-link"> <i class="nav-icon fas fa-user"></i>
          <p> Profile <i class="fas fa-angle-left right"></i> </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item"> <a href="/admin/profile" class="nav-link"> <i class="far fa-circle nav-icon"></i>
              <p>Update Profile</p>
              </a> </li>
            <li class="nav-item"> <a href="/admin/profile/change-password" class="nav-link"> <i class="far fa-circle nav-icon"></i>
              <p>Change Password</p>
              </a> </li>
          </ul>
        </li>
        <li class="nav-item"> <a href="/admin/user" class="nav-link"> <i class="nav-icon far fa-user"></i>
          <p>Users</p>
          </a> </li>
        <li class="nav-item"> <a href="/admin/logout" class="nav-link"> <i class="nav-icon far fa-circle text-info"></i>
          <p>Logout</p>
          </a> </li>

      </ul>
    </nav>
    <!-- /.sidebar-menu --> 
  </div>
  <!-- /.sidebar --> 
</aside>
