<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\LoginController;
use App\Http\Controllers\Admin\Dashboard;
use App\Http\Controllers\Admin\SitepageController;
use App\Http\Controllers\Admin\ProfileController;
use App\Http\Controllers\Admin\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('admin/login');
});

Route::get('admin/login', function () {

	if(session()->has('userId'))
    {
    	  return redirect('admin/dashboard');
    }
    return view('admin/login');
});
Route::post('admin/login', [LoginController::class, 'login']);

Route::get('admin/dashboard', function () {

	if(!session()->has('userId'))
    {
    	  return redirect('admin/login');
    }
    
    return view('admin/dashboard');
});

Route::get('admin/logout', function () {
    if(session()->has('userId'))
    {
    	session()->pull('userId',null );
    	session()->pull('userEmail',null );
    }

    return redirect("admin/login")->with('logoutMessage', 'Logout Successful');
});

Route::get('admin/sitepage/list', [SitepageController::class, 'list']);

Route::get('admin/sitepage/updateStatus/{id}/{status}', [SitepageController::class, 'updateStatus']);
Route::view('admin/sitepage/add-new', 'admin/sitepage/add');
//Route::view('admin/sitepage/add-new', [SitepageController::class, 'add']);

Route::post('admin/sitepage/add-new', [SitepageController::class, 'addNew']);

Route::get('admin/sitepage/edit/{id}', [SitepageController::class, 'edit']);
Route::post('admin/sitepage/updatePage/{id}', [SitepageController::class, 'updatePage']);
Route::get('admin/sitepage/delete/{id}', [SitepageController::class, 'delete']);

Route::get('admin/profile', [ProfileController::class, 'adminProfile']);
Route::post('admin/profile/update/{id}', [ProfileController::class, 'update']);

Route::get('admin/profile/change-password', [ProfileController::class, 'changePassword']);
Route::post('admin/profile/updatePassword', [ProfileController::class, 'updatePassword']);
Route::get('admin/profile/removeImage/{id}', [ProfileController::class, 'removeImage']);

Route::get('admin/user/list', [UserController::class, 'list']);
Route::view('admin/user/add', 'admin/user/add');
Route::post('admin/user/add-new', [UserController::class, 'addNew']);
Route::get('admin/user/edit/{id}', [UserController::class, 'edit']);
Route::post('admin/user/updatePage/{id}', [UserController::class, 'updatePage']);
Route::get('admin/user/removeImage/{id}', [UserController::class, 'removeImage']);
Route::get('admin/user/delete/{id}', [UserController::class, 'delete']);
Route::get('admin/user/updateStatus/{id}/{status}', [UserController::class, 'updateStatus']);
